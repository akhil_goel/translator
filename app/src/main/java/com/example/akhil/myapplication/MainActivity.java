package com.example.akhil.myapplication;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.internal.widget.AdapterViewCompat;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends ActionBarActivity implements AdapterView.OnItemSelectedListener {
    GoogleTranslate translator;
    EditText translateedittext;
    TextView translatabletext;
    Spinner langSpinner;
    String lang, shortLang = "";
    SharedPreferences pref;
    SharedPreferences.Editor edit;
    Button translatebutton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pref = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        edit = pref.edit();
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        translateedittext = (EditText) findViewById(R.id.translateedittext);
        langSpinner = (Spinner) findViewById(R.id.spinnerLang);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.lang_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        int spinnerPosition = adapter.getPosition(pref.getString("LangPref", ""));
        langSpinner.setAdapter(adapter);
        langSpinner.setSelection(spinnerPosition);
        langSpinner.setOnItemSelectedListener(this);
        translatebutton = (Button) findViewById(R.id.translatebutton);
        translatebutton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                new EnglishToTagalog().execute();
            }
        });
    }

    public void translated() {
        String translatetotagalog = translateedittext.getText().toString();//get the value of text
        switch (lang) {
            case "Hindi":
                shortLang = "hi";
                break;
            case "Gujarati":
                shortLang = "gu";
                break;
            case "French":
                shortLang = "fr";
                break;
            case "Spanish":
                shortLang = "es";
                break;
            case "Tamil":
                shortLang = "ta";
                break;
            case "":
                shortLang = "";
                break;
            default:
                shortLang = "";
        }
        String text = translator.translte(translatetotagalog, "en", shortLang);
        translatabletext = (TextView) findViewById(R.id.translatabletext);
        translatabletext.setText(text);
        String text1 = translatebutton.getText().toString();
        text = translator.translte(text1, "en", shortLang);
        translatebutton.setText(text);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        lang = (String) adapterView.getItemAtPosition(i);
        edit.putString("LangPref", lang);
        edit.commit();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
        lang = "";
    }

    private class EnglishToTagalog extends AsyncTask<Void, Void, Void> {
        private ProgressDialog progress = null;

        protected void onError(Exception ex) {
            Toast.makeText(MainActivity.this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            try {
                translator = new GoogleTranslate("AIzaSyC_ilhQ12Vp_Y4UOdgRmZdSZmFTfOeW2UI");
                Thread.sleep(300);

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return null;

        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
        }

        @Override
        protected void onPreExecute() {
            //start the progress dialog
            progress = ProgressDialog.show(MainActivity.this, null, "Translating...");
            progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progress.setIndeterminate(true);
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void result) {
            progress.dismiss();
            super.onPostExecute(result);
            translated();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

    }
}

